from __future__ import print_function
from operator import itemgetter
import random
import numpy
import pickle

N = 9

def painted(i, j):
    return mat[i][j] == '#'


def square_sore(r1, c1, r2, c2):
    score = 0
    for i in range(r1, min(r2 + 1, nr - 1)):
        for j in range(c1, min(c2 + 1, nc - 1)):
            score += 1 if painted(i, j) else -1
    return score


def compute_instr():
    global nr, nc, mat

    # def find(nn, minc=1000000, coups=None):
    #     if nn == 0:
    #         return coups

    instr = []
    N2 = N//2
    NN = N**2
    NN2 = NN//2

    print(N, N2, NN)

    # O..
    # .C.
    # ...

    for j in range(0, nr - N, N):
        for k in range(0, nc - N, N):
            instg = []

            toerase = []
            topaint = []
            for n in range(N):
                for m in range(N):
                    if painted(j+n, k+m):
                        topaint.append((j+n, k+m))
                    else:
                        toerase.append((j+n, k+m))

            if len(topaint) <= NN2 + 1:
                for n, m in topaint:
                    instg.append('PAINTSQ {r} {c} {s}'.format(r=min(n, nr-1), c=min(m, nc-1), s=0))
                continue

            if len(topaint) > 1:
                instg.append('PAINTSQ {r} {c} {s}'.format(r=min(j+N2, nr-1), c=min(k+N2, nc-1), s=N2))
                for n, m in toerase:
                    instg.append('ERASECELL {r} {c}'.format(r=min(n, nr-1), c=min(m, nc-1)))

            instr.append(instg)

    # some lol
    random.shuffle(instr)
    # yeah
    return [item for sublist in instr for item in sublist]


def main(fin, fout):
    global nr, nc, mat

    with open(fin, 'r') as f:
        data = [_ for _ in f.read().split('\n') if _]

    nr, nc = map(int, data.pop(0).split())
    mat = [list(_) for _ in data]

    print("size is %d x %d" % (nr, nc))

    instr = compute_instr()

    with open(fout, 'w') as f:
        f.write('%d\n' % len(instr))
        f.write('\n'.join(instr))


if __name__ == '__main__':
    # Usage: main.py FILE_IN FILE_OUT
    import sys
    main(sys.argv[1], sys.argv[2])
