from __future__ import print_function
from operator import itemgetter
import numpy
import pickle

def painted(i, j):
    return mat[i][j] == '#'


def square_score(r1, c1, r2, c2):
    score = 0
    for i in range(r1, min(r2 + 1, w - 1)):
        for j in range(c1, min(c2 + 1, h - 1)):
            score += 1 if painted(i, j) else -1
    return score


def compute_instr():
    global cases
    instr = []

    taban = []

    #cases = numpy.zeros((w, h), dtype=tuple)

    for j in range(0, w - 1):
        for k in range(0, h - 1):
            i = 2
            lasti = 2
            lastval = 0
            while True:
                # print("%d,%d: %d" % (j, k, i))
                val = square_score(j, k, j + i, k + i)
                if val < 0 or val < lastval:
                    break
                lasti = i
                lastval = val
                i += 2

            if lastval > 0:
                taban.append((lastval, lasti, j, k))
            elif lastval < 0:
                if painted(j, k):
                    taban.append((0, 0, j, k))

    with open('/tmp/pickled', 'wb') as f:
        pickle.dump(taban, f)
    taban.sort(reverse=True, key=itemgetter(0))

    done = set()

    for val, i, j, k in taban:
        if (j, k) in done:
            continue
        centerx = j + i//2
        centery = k + i//2
        ins = 'PAINTSQ %d %d %d' % (
            centerx, centery, i//2
        )
        instr.append(ins)
        for jj in range(j, min(w, j + i)):
            for kk in range(k, min(h, k + i)):
                if not painted(jj, kk):
                    ins = 'ERASECELL %d %d' % (jj, kk)
                    instr.append(ins)
                else:
                    done.add((jj, kk))

    return instr


def main(fin, fout):
    global w, h, mat

    with open(fin, 'r') as f:
        data = [_ for _ in f.read().split('\n') if _]

    w, h = map(int, data.pop(0).split())
    mat = [list(_) for _ in data]

    print("size is %d x %d" % (w, h))

    instr = compute_instr()

    with open(fout, 'w') as f:
        f.write('%d\n' % len(instr))
        f.write('\n'.join(instr))


if __name__ == '__main__':
    # Usage: main.py FILE_IN FILE_OUT
    import sys
    main(sys.argv[1], sys.argv[2])
