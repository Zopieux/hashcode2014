from __future__ import print_function
from operator import itemgetter
import math

NS = 8
FACTOR = 10000

def main(fin):
    with open(fin, 'r') as f:
        data = [_ for _ in f.read().split('\n') if _]

    n, m, maxt, ncars, start = map(int, data.pop(0).split())
    nodes = []
    node_pos_cart = []

    for ii in range(0, n):
        lat, lon = map(float, data[ii].split())
        nodes.append((lat, lon))
        #x = 6371*math.cos(lat) * math.cos(lon)
        #y = 6371*math.cos(lat) * math.sin(lon)
        #z = 6371 * math.sin(lat)
        #node_pos_cart.append((x, y,z))
        #print (data[ii])


    minx = min(map(itemgetter(0), nodes))
    miny = min(map(itemgetter(1), nodes))
    maxx = max(map(itemgetter(0), nodes))
    maxy = max(map(itemgetter(1), nodes))
    #maxz = max(map(itemgetter(2), node_pos_cart))
    #minz = min(map(itemgetter(2), node_pos_cart))

    centerx  = (maxx+minx)/2.
    centery  = (maxy+miny)/2.
    #centerz  = (maxz-minz)/2.
    print(maxx,maxy)

    print(centerx, centery)

    polars = []
    maxr = -100000000
    for x, y in nodes:

        x=x-centerx
        y=y-centery
        r = math.sqrt(x**2 + y**2)
        if x > 0 and y >= 0:
            teta = math.atan(y/x)
        elif x > 0 and y < 0:
            teta = math.atan(y/x) + 2 * math.pi
        elif x < 0:
            teta = math.atan(y/x) + math.pi
        elif x == 0 and y > 0:
            teta = math.pi / 2.
        else:
            teta = 3. * math.pi / 2.

        maxr = max(maxr, r)
        polars.append((r, teta))
        # print(r,teta)

    # from PIL import Image, ImageDraw
    # size = (int(centerx*2*FACTOR), int(centery*2*FACTOR))

    # im = Image.new('RGB', size, color='white')
    # draw = ImageDraw.Draw(im)

    sectors = []
    sectorsset = set()
    mt = 0
    for r, teta in polars:
        mt = max(mt, teta)
        sector = int(teta // (2 * math.pi / NS))
        sectorsset.add(sector)
        sectors.append(sector)

        # x = r * math.cos(teta) - minx
        # y = r * math.sin(teta) - miny
        # x *= centerx*2*FACTOR
        # y *= centery*2*FACTOR
        # x=int(x)
        # y=int(y)
        #print(x, y)
        # draw.point([(x,y)], fill=(0,0,0))

    assert sectorsset == {0,1,2,3,4,5,6,7}
    # del draw
    # im.show()
    import pickle
    with open('sectors.pick','wb') as f:
        pickle.dump(sectors, f)

if __name__ == '__main__':
    # Usage: main.py FILE_IN FILE_OUT
    import sys
    main(sys.argv[1])
