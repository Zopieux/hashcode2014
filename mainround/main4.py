from __future__ import print_function
from operator import itemgetter
import numpy
import pickle
import random
from collections import defaultdict
from dijkstra import shortestPath


def main(fin, fout):
    global n, m, maxt, ncars, start, roads, roads_start, paths, total_score

    with open(fin, 'r') as f:
        data = [_ for _ in f.read().split('\n') if _]

    n, m, maxt, ncars, start = map(int, data.pop(0).split())
    #mat = [list(_) for _ in data]

    roads = {}
    roads_start = defaultdict(lambda: [])

    def sortage():
        global best_inter, roads_start
        best_inter = []
        for ss, roadss in roads_start.items():
            score = sum(l/c for c,l,d in roadss)
            best_inter.append((score, ss))
        best_inter.sort(reverse=True)

    # for ii in range(0, n):

    for ii in range(n, n+m):
        a, b, double, cc, l = map(int, data[ii].split())

        tup = [float(cc), float(l)]
        if cc > maxt:
            print("ignoring road %r" % tup)
            continue

        roads[(a, b)] = tup
        roads_start[a].append(tup + [b])

        if double == 2:
            roads[(b, a)] = tup
            roads_start[b].append(tup + [a])

    print("there are %d roads" % len(roads))
    print("%d roads from start (%d)" % (len(roads_start[start]), start))

    sortage()
    # random.shuffle(best_inter)

    # print(best_inter[:ncars])
    # compute_paths()
    # print(total_score)
    graph = {}

    for k, roadss in roads_start.items():
        graph[k] = {d: c/(1+l)/(1+len(roadss)) for c,l,d in roadss}

    print(len(graph))

    total_score = 0
    paths = []
    best_end = 0
    for car in range(ncars):
        realpath = []
        dstart = start
        cost = 0
        cont = True
        first = True

        while cont:
            if first:
                dend = best_inter.pop(0)[1]
                print("car %d: dijkstra %d -> %d" % (car, dstart, dend))
                path = shortestPath(graph, dstart, dend)
                if len(path) == 0:
                    break
            else:
                item = random.choice(best_inter)
                best_inter.remove(item)
                dend = item[1]

            realpath.append(path[0])
            for i in range(len(path) - 1):
                step, step2 = path[i], path[i+1]
                c, l = roads[(step, step2)]

                # for i, (c,l,d) in enumerate(roads_start[step]):
                #     if d == step2:
                #         roads_start[step][i][1] = 0
                # for i, (c,l,d) in enumerate(roads_start[step2]):
                #     if d == step:
                #         roads_start[step2][i][1] = 0
                # sortage()

                if step2 in graph[step]:
                    graph[step][step2] = 1000000000000
                if step in graph[step2]:
                    graph[step2][step] = 1000000000000

                cost += c

                if cost > maxt:
                    cont = False
                    break

                total_score += l
                realpath.append(step2)

            else:
                # we did it to the end!

                # add last?
                # c, l = roads[(path[-2], path[-1])]
                # dstart = path[-1]
                # if len(path) % 2 == 0:
                realpath.pop(-1)
                if first:
                    first = False
                dstart = dend
                # best_end += 1
                # if cost + c <= maxt:
                    # total_score += l
                    # realpath.append(path[-1])

        print(cost)
        # print(realpath)
        paths.append(realpath)

    print(total_score)

    with open(fout, 'w') as f:
        f.write('%d\n' % ncars)
        for carpath in paths:
            f.write('%d\n' % len(carpath))
            for step in carpath:
                f.write('%d\n' % step)


if __name__ == '__main__':
    # Usage: main.py FILE_IN FILE_OUT
    import sys
    main(sys.argv[1], sys.argv[2])
