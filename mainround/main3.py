from __future__ import print_function
from operator import itemgetter
import numpy
import pickle
import random
from collections import defaultdict

with open('sectors.pick','rb') as f:
    SECTORS = pickle.load(f)


def all_equal(li):
    ll = len(li)
    if ll <= 1:
        return True
    cc, ll = li[0][0], li[0][1]

    for c, l, g in li:
        if c != cc or l != ll:
            return False
    return True


def rscore(id, tcost, tlength, dest):
    global maxt, ncars, start, roads, roads_start, paths, car_cost, total_score, last_inter, visited_inter

    weight = tlength / tcost * 2.5
    # weight = 0

    ahead = [l/c for
        c,l,d in roads_start[dest]]
    ahead_score = max(ahead)

    weight += ahead_score

    if id == SECTORS[dest]:
        weight += 1

    # if dest == last_inter[id]:
    #     weight -= 100000
    if dest in visited_inter:
        weight -= 1

    return (weight, random.randint(0, 10000000))


def explore(id):
    global maxt, ncars, start, roads, roads_start, paths, car_cost, total_score, last_inter

    while True:
        imhere = paths[id][-1]
        visited_inter.add(imhere)
        avail = roads_start[imhere][:]

        l = len(avail)
        if l == 0:
            print("%d is stuck at %d" % (id, imhere))
            break

        # if all_equal(avail):
        #     print("all equal")
        #     random.shuffle(avail)
        # else:
        # which is best?
        avail.sort(key=lambda r: rscore(id, *r), reverse=True)

        for av in avail:
            tcost, tlength, goto = av
            # if l > 1 and goto == last_inter[id]:
            #     continue
            if car_cost[id] + tcost <= maxt:
                break
        else:
            # none found that does not exceed maxt, so break main loop to exit
            break

        # set length = 0 on the road i take
        for i, (c, l, g) in enumerate(roads_start[imhere]):
            if g == goto:
                roads_start[imhere][i] = [c, 0, g]

        for i, (c, l, g) in enumerate(roads_start[goto]):
            if g == imhere:
                roads_start[goto][i] = [c, 0, g]

        # go to my destination
        paths[id].append(goto)
        last_inter[id] = imhere
        # and add cost
        car_cost[id] += tcost
        # and add score
        total_score += tlength

        #print("%d go to %d" % (id, goto))
    # and continue explore
    # explore(id)


def compute_paths():
    global maxt, ncars, start, roads, roads_start, paths, car_cost, total_score, \
        visited_inter, last_inter, inter_pos

    total_score = 0
    car_cost = [0 for _ in range(ncars)]
    paths = [[start] for _ in range(ncars)]
    last_inter = paths[:]
    visited_inter = set()

    for car in range(ncars):
        explore(car)


def main(fin, fout):
    global n, m, maxt, ncars, start, roads, roads_start, paths, total_score

    with open(fin, 'r') as f:
        data = [_ for _ in f.read().split('\n') if _]

    n, m, maxt, ncars, start = map(int, data.pop(0).split())
    #mat = [list(_) for _ in data]

    roads = {}
    roads_start = defaultdict(lambda: [])

    # for ii in range(0, n):


    for ii in range(n, n+m):
        a, b, double, cc, l = map(int, data[ii].split())

        tup = [float(cc), float(l)]
        if cc > maxt:
            print("ignoring road %r" % tup)
            continue

        roads[(a, b)] = tup
        roads_start[a].append(tup + [b])

        if double == 2:
            roads[(b, a)] = tup
            roads_start[b].append(tup + [a])

    print("there are %d roads" % len(roads))

    print("%d roads from start (%d)" % (len(roads_start[start]), start))

    compute_paths()
    print(total_score)

    with open(fout, 'w') as f:
        f.write('%d\n' % ncars)
        for carpath in paths:
            f.write('%d\n' % len(carpath))
            for step in carpath:
                f.write('%d\n' % step)


if __name__ == '__main__':
    # Usage: main.py FILE_IN FILE_OUT
    import sys
    main(sys.argv[1], sys.argv[2])
