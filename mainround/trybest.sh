#!/bin/bash

file=$1
core=$2

tmp=/tmp/$file.$core.res

bestfile=/tmp/$file.best.score
actual=./result/$file.best.res

bests=$(cat $bestfile)
if [ $? -ne 0 ]; then
	bests=0
fi

while true; do
	out=$(python $file paris_54000.txt $tmp | tail -n1)
	if [ $out -gt $bests ]; then
		bests=$out
		echo $bests>$bestfile
		cp $tmp $actual
		echo "FOUND BEST!!! $bests"
	fi
done
